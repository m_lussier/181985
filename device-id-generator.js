import uuid from 'uuid/v4';

class DeviceIdGenerator {
  static getDeviceId = () => Promise.resolve(uuid())
}

export default DeviceIdGenerator;